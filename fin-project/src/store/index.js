import Vue from 'vue'
import Vuex from 'vuex'
import blogs from './modules/blogs'
import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: 'siapaja',
  Storage: localStorage
})


Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  state: {
    guest: true,
    user: {},
    token: ''
  },
  actions: {
    setGuest({ commit }, value) {
      commit('setGuest', value)
    },
    setUser({ commit }, payload) {
      commit('setUser', payload)
    },
    setToken({ commit }, payload) {
      commit('setToken', payload)
    }
  },
  mutations: {
    setGuest(state, value) {
      state.guest = value
    },
    setUser(state, payload) {
      state.user = payload
    },
    setToken(state, payload) {
      state.token = payload
    }
  },
  modules: {
    blogs,
  }
})
